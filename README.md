This is docker swarm monitoring system using Prometheus, Cadvisor, and Grafana packed into docker container.

## Installation
Ensure you already have docker swarm initiated on your machine or cluster.

change the required file name
```sh
cp cadvisor-scrape.yml.copy prometheus.yml
cp monitoring.yml.copy monitoring.yml
```
Change the prometheus conf path file in monitoring.yml prometheus volume section
```sh
/path/to/your/prometheus.yml_file
```

Deploy the container from swarm manager machine or some app like swarmpit or portainer

```sh
docker stack deploy -c monitoring.yml
```

Visit the service on your browser

**Prometheus UI**
```sh
localhost:9090
```
**Grafana UI**
```sh
localhost:3000
```
**Cadvisor UI**
```sh
localhost:8080/docker
```
